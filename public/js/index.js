
var socket;
var files_length = 0;
var mount_path;
var files_array;
var check_status = false;
var user_name;

var main_upload_status;

$(function () {

	// getting the socket
	socket = io();

	socket.emit('check_dropbox_auth', function(data) {
		if(data) {
			console.log(data);
			console.log("logged in");
			user_name = data
			$("#dropbox_auth_items").hide();
			$("#dropbox_logged_in").show();
			$("#dropbox_login_info").html("logged in as: " + user_name)


		} else {
			$("#dropbox_auth_items").show(); 
			$("#dropbox_logged_in").hide(); 
			alert("Please Setup Dropbox Account");
		}
	});

	// check the usb mount and show loading
	check_usb_status(1);

	get_usb_files();

	setInterval(checkups, 500);
	setInterval(function(){
		socket.emit('check_autoupload', function(data) {
			if(data === false){
				$("#enable_autoupload").prop('checked', false)

			}else{
				$("#enable_autoupload").prop('checked', true)

			}
		});
		socket.emit('check_upload_only_today', function(data) {
			if(data === false){
				$("#enable_upload_from_today").prop('checked', false)

			}else{
				$("#enable_upload_from_today").prop('checked', true)

			}
		});

	}, 2000);

	function loading(display, text) {
	if(display) {
		console.log("Displaying loading...");
		$("#loading_screen").show();
		$('body').css({'overflow':'hidden'});
		$('html,body').scrollTop(0);
	} else {
		console.log("Removing loading...");
		$("#loading_screen").hide();
		$(document).unbind('scroll'); 
		$('body').css({'overflow':'visible'});
	}

	$("#loading_status").text(text);
}

/*
 * Function: check_usb_status(display)
 * Description: it checks the status of the usb (mounted?)
 * @param display - show the loading when checking
 */
function check_usb_status() {
	socket.emit('usb', "mount");
	socket.on('usb_mount', function(data) {
		console.log(data.path);
		if(data.stat == 1) {
		console.log("Everything okay, usb detected");
	} else {
		console.log("Something went wrong...");
		alert("No USB detected");
	}
});
}

/*
 *  Function: get_usb_files()
 *  Description: act upon the files we have on the usb
 */
function get_usb_files() {

	socket.emit('files', "get_files");
	socket.on('files', function(data) {
		files_array = data.array_data
		files_length = (files_array).length;					
		$("#image_counter").html(files_length);
		for(i=0; i < files_length; i++) {
			if(files_array[i] != "") {
				$("#file_group").append('<li class="list-group-item">' + files_array[i] + '<p style="float:right">Upload <input type="checkbox" id="all_upload'+ i + '"checked> Resize <input type="checkbox" id="all_resize'+ i + '"></p></li>');
			}
		}
	});
}

/*
 * Function: check_all(state)
 * Description: set the state of all the checkboxes
 * @param - state -> the state of all the checkboxes
 */
function check_all(begin_id, state) {
	//console.log(files_length);
	for(i=0; i < files_length; i++) {
		$(begin_id + i).prop('checked',state);
	}
}

//TODO Gets Saved Effects
function getEffectsFromSaved() {
		socket.emit('get_effects', "get_files");
		socket.on('get_effects', function(data) {})
}

$("#enable_upload_from_today").click(function(){
	console.log("click")
	if($("#enable_upload_from_today").is(':checked')){
		console.log("enablinguploadfromtoday");
		socket.emit('enable_upload_only_today');				
	}else if($("#enable_upload_from_today").not(':checked')){
		console.log("disenablinguploadfromtoday");
		socket.emit('disable_upload_only_today');
	}
});

$("#enable_autoupload").click(function(){
	console.log("click")
	if($("#enable_autoupload").is(':checked')){
		console.log("enablingautoupload");
		socket.emit('enable_autoupload');				
	}else if($("#enable_autoupload").not(':checked')){
		console.log("disenablingautoupload");
		socket.emit('disable_autoupload');
	}
});

function getEffects(){
	var effects = {}
	if($("#enhance_image").is(':checked')){
		effects["enhance"]="true"
	}
	if($("#enable_contrast").is(':checked')){
		effects["contrast"] = $("#image_contrast").val() 
	
	}
	if($("#enable_saturation").is(':checked')){
		effects["saturation"] = $("#image_saturation").val() 
	}
	if($("#enable_shadows").is(':checked')){
		effects["shadows"] = $("#image_shadows").val() 
	}
	if($("#enable_highlights").is(':checked')){
		effects["highlights"] = $("#image_highlights").val() 
	}
	if($("#only_upload_last_day_images").is(':checked')){
		effects["upload_last_day"] = $("#image_highlights").val() 
	}
	console.log("contrast: "+$("#image_contrast").val());
	console.log("saturation: "+$("#image_saturation").val());
	console.log("shadows: "+$("#image_shadows").val());
	console.log("highlishts: "+$("#image_highlights").val());
	console.log(effects.contrast);
	return effects;
}

$("#save_effects").click(function() {
	var effects = getEffects()
	console.log(effects)
	socket.emit('save_effects', effects);				
});

$("#upload").click(function() {

	loading(1, "Starting upload...");			

	var upload_data = {
		main_upload_files : [],
		effects : {}
	};

	for(i=0; i < files_length; i++) {
		upload_data.main_upload_files.push(files_array[i]);
	}


	if(upload_data.main_upload_files.length == 0) {
		alert("No files selected!");
		loading(0, "");
	} else {

		console.log(upload_data.main_upload_files);

		check_status = true;

		socket.emit('upload', upload_data);				
	}	
});			

$("#setup_dropbox").click(function() {
	socket.emit('get_dropbox_auth', function(data) {
		console.log(data)
		window.location = data
	});

});			

$("#logout_dropbox").click(function() {
	socket.emit('logout_dropbox', function(data) {
		console.log(data)
		window.location = "/";
	});
});
function checkups() {
	//console.log("Checking...");
	if(check_status) {
		
		socket.emit('status', function(data) {
			if(main_upload_status != data.main_upload) {
				main_upload_status = data.main_upload;
				loading(1, main_upload_status);
				if(main_upload_status == "Finished") {
					check_status = false;
					alert("Upload finished, check your dropbox!");
					loading(0, "");
				}
			}
		});
	}

	//console.log(main_upload_status);
};
});
