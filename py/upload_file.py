import dropbox, json, sys, os, pdb
with open('./doc/secret.json') as json_file:  
        dropbox_info = json.load(json_file)
dbx = dropbox.Dropbox(dropbox_info['token'])

path = sys.argv[1]

def upload_file(file_path):
#   print(file_path)
#   with open(file_path, 'rb') as f:
#       dbx.files_upload(f.read(), "/dbloader/Original/"+file_name, mode= dropbox.files.WriteMode.overwrite)
#   print("file uploaded")
    print(file_path)
    file_name = os.path.basename(file_path)
    dest_path = "/dbloader/Original/"+file_name
    f = open(file_path)
    file_size = os.path.getsize(file_path)

    CHUNK_SIZE = 4 * 1024 * 1024

    if file_size <= CHUNK_SIZE:
        print dbx.files_upload(f.read(), dest_path)

    else:

        upload_session_start_result = dbx.files_upload_session_start(f.read(CHUNK_SIZE))
        cursor = dropbox.files.UploadSessionCursor(session_id=upload_session_start_result.session_id,
                                                   offset=f.tell())
        commit = dropbox.files.CommitInfo(path=dest_path)

        while f.tell() < file_size:
            print(f.tell()-file_size)
            if ((file_size - f.tell()) <= CHUNK_SIZE):
                print dbx.files_upload_session_finish(f.read(CHUNK_SIZE), cursor, commit)
            else:
                dbx.files_upload_session_append(f.read(CHUNK_SIZE), cursor.session_id,  cursor.offset)
                cursor.offset = f.tell()
try:
    upload_file(path)
except Exception as e:
    print e



