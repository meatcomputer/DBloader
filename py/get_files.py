import os
import sys

path = sys.argv[1]

file_extensions = ("jpg", "png", "tiff", "JPG", "JPEG", "TIFF", "mov","MOV", "mp4", "MP4", "avi", "AVI","mkv","MKV","mp3","MP3","flac","FLAC")

for root, dirs, files in os.walk(path):                                                                   #walking on video_path
	for file in files: 
            if not file.startswith('.') and file.endswith(file_extensions):
			sys.stdout.write(os.path.join(root, file))
			sys.stdout.write("\n")
