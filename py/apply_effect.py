#Takes an Image And Applies Effects to them

import PIL, json, sys, os
from PIL import Image
from PIL import ImageEnhance

with open('./effects.json') as json_file:  
        effects = json.load(json_file)


path = sys.argv[1]
basename = os.path.basename(path)
final_img = None

print(path)
print(basename)
def change_contrast(image_path, level):
        #0-255
       img = PIL.Image.open(image_path)
       factor = (259 * (level + 255)) / (255 * (259 - level))
       def contrast(c):
           return 128 + factor * (c - 128)
       return img.point(contrast)

def enhance_image(image_path):
    #https://stackoverflow.com/questions/16070078/change-saturation-with-imagekit-pil-or-pillow
    img = PIL.Image.open(image_path)
    converter = PIL.ImageEnhance.Color(img)
    img2 = converter.enhance(0.5)
    return img2

if "enhance" in effects:
    enhanced_img = enhance_image(path)
    final_img = enhanced_img
if "contrast" in effects:
    contrast_img = change_contrast(path,int(effects["contrast"]))
    final_img = contrast_img

if final_img:
    final_img.save("/tmp/"+basename)
    sys.stdout.write("/tmp/"+basename)
    sys.exit()
else:
    sys.stdout.write(path)
    sys.exit()



