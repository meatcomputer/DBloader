var Dropbox = require('dropbox');

var index = require('../index.js');
var path = require('path');
var secret = require('../doc/secret.json');
var fs = require('fs');
var OneDay = new Date().getTime() - (1 * 24 * 60 * 60 * 1000)


var current_status;

var counter = 0;
const execSync = require('child_process').execSync;

function uploadToDropbox(dbx, file_name, contents, selected_data){
		dbx.filesUpload({path: '/dbloader/Original/' + file_name, contents : contents, autorename: true})
			.then(function(response) {
				console.log(response);
				console.log("Uploaded: " + response.path_display);
				current_status = response.path_display + " ✔";
				counter = counter + 1;
				console.log(selected_data.length);
				console.log(counter);
				if(selected_data.length == counter) {
					current_status = "Finished";
				}
				return "done"
			})
			.catch(function(error) {
				counter = counter + 1
				current_status = "Error! This is the Error: "+error.error;
				return "error"
			});
}

module.exports = {
	main_upload: function(data) {
		var dbx = new Dropbox({ accessToken: secret.token });
		current_status = "s";
		data.forEach( 
			function(raw_filename){
				try {
					console.log("processin: ");

					console.log(raw_filename)
					file_name = path.basename(raw_filename).toString();
					var stats = fs.statSync(raw_filename);
					console.log(stats.mtime);
				} catch(err){
					console.log(err);
					return;

				}
				var uploadOnlyToday = fs.existsSync("uploadonlytoday")
				if (uploadOnlyToday && OneDay > stats.mtime.getTime()){
					console.log("file too old")
					return;
				}

				//console.log("this is what I spawned: "+ 'python /home/pi/DBloader/py/apply_effect.py "'+raw_filename+'"')
				result = execSync('python /home/pi/DBloader/py/apply_effect.py "'+raw_filename+'"')
				console.log("heres the child")
				console.log(result.toString().split("\n").slice(-1)[0])
				effect_path = result.toString().split("\n").slice(-1)[0]

				if(effect_path === "false"){
					current_file = raw_filename;
				}else{
					current_file = effect_path;
				}

				upload_result = execSync('python /home/pi/DBloader/py/upload_file.py "'+current_file+'"')
				
				return upload_result;

			})
		}
	,
	main_upload_status: function(status) {
		status(current_status);
	}
	
}
