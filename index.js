var utils = require(__dirname + '/js/utils.js');
var express = require('express');
var app = require('express')();
var htp = require('http')
var bodyParser = require('body-parser');
var http = htp.Server(app);
var io = require('socket.io')(http);
var url = require('url');
var json = require('json-update');
var fs = require('fs');

const https = require('https');
const sslOptions = {
	    key: fs.readFileSync(__dirname+"/cert/server.key"),
	    cert: fs.readFileSync(__dirname+"/cert/server.cer")
};
https.createServer(sslOptions, app).listen(3001);

var Dropbox = require('dropbox');

var secret = require(__dirname + '/doc/secret.json'); // where the appKey is ;)

var dir = require(__dirname + '/js/dir.js');
var dir_driver = require(__dirname + '/driver/dir_driver.js')

var main_upload = require(__dirname + '/dbloader/main_upload.js');

app.use(bodyParser.json());
app.use(express.static('public'))

//Routes
app.get('/', function(req, res){ res.sendFile(__dirname + '/views/index.html'); });
app.get('/oauthredirect', function(req, res){ res.sendFile(__dirname + '/views/oauthredirect.html'); });

//Post comes from oauthredirect.html
app.post('/token', function(req, res){  
	console.log('body: ' + JSON.stringify(req.body));
	res.send(req.body);
	token = req.body.token
	json.update(__dirname + '/doc/secret.json', {token : token});
});

function get_secret(){
	raw_data = fs.readFileSync(__dirname+'/doc/secret.json');
	secret = JSON.parse(raw_data);
	return secret;
}


function autoUploadEnabled(callback){
	fs.exists("autoupload",function(resp){
		callback(resp)
	})
}
 function getEffects(){
	raw_data = fs.readFileSync("effects.json");
	effects = JSON.parse(raw_data);
	return effects;
}
function saveEffects(effects){
	try {
		fs.writeFileSync("effects.json", JSON.stringify(effects))
	} catch (err) {
		console.error(err)
	}

}
function enableAutoUpload(){
	autoUploadEnabled(function(response){
		if(response === false){
			fs.open("autoupload", "w", function(resp){})	
		}
	});
}	
function disableAutoUpload(){
	fs.unlink("autoupload", function(resp){})	
}

function uploadOnlyTodayEnabled(callback){
	fs.exists("uploadonlytoday",function(resp){
		callback(resp)
	})
}
function enableUploadOnlyToday(){
	uploadOnlyTodayEnabled(function(response){
		if(response === false){
			fs.open("uploadonlytoday", "w", function(resp){})	
		}
	});
}
function disableUploadOnlyToday(){
	fs.unlink("uploadonlytoday", function(resp){})	
}
function uploadAllFiles(){
        dir_driver.get_files(dir_driver.mount_path, function(files) {
        	main_upload.main_upload(files);		
        });
}


io.on('connection', function(socket){

	/*
	 *  Communicating with index.html about the usb 
	 */
	socket.on('usb', function(msg) {
		if(msg == "mount") { // if we are talking about mounting
			dir.get_usb_dev_path(function(usb_dev_path){
				dir.setup_usb(function(status) {
					// returning data about the status and the dev path of the usb
					socket.emit('usb_mount', {stat : status, path : usb_dev_path });
				});
			});
		}
	});

	/*
         *  Communicating with index.html about the files
         */ 
	socket.on('files', function(type) {
		if(type == "get_files") {
			dir_driver.get_files(dir_driver.mount_path, function(data) {
				socket.emit('files', { array_data : data });
			});
		} else if(type == "mount_folder") {
			socket.emit('files', { mount_folder : dir_driver.mount_path});
		}
	});
	
	
	
	socket.on('check_dropbox_auth', function(msg) { 
			secret = get_secret()
			var dbx = new Dropbox({ accessToken: secret.token });
			dbx.usersGetCurrentAccount().
				then(function(response){
					var user_info = response;
					msg(user_info.email);
				}).
				catch(function(error){ 
					console.error(error);
					msg(0);
				});       	

	});
	
	socket.on('get_dropbox_auth', function(return_url) {
		var dbx = new Dropbox({ clientId: secret.appKey });
		var url = dbx.getAuthenticationUrl("https://dbloader.local:3001/oauthredirect");
		console.log(url);
		return_url(url);
	});
	socket.on('logout_dropbox', function(data) {
		json.update(__dirname + '/doc/secret.json', {token : false});
		data("logged out");
	});
	
	
	
	socket.on('upload', function(data) {
		//console.log(data.main_upload_files);
		main_upload.main_upload(data.main_upload_files);		
	});
	
	socket.on('status', function(data) {
		data({main_upload : main_upload_status});
	});

	socket.on('save_effects', function(data) {
		saveEffects(data)
	})
	socket.on('get_effects', function(data) {
		effects = getEffects()
		socket.emit('effects', { effects : effects });
	})
	socket.on('enable_autoupload', function(data) {
		enableAutoUpload()
	})
	socket.on('disable_autoupload', function(data) {
		disableAutoUpload()
	})
	socket.on('check_autoupload', function(data) {
		autoUploadEnabled(function(response){
			data(response);
			
		});
	});
	socket.on('enable_upload_only_today', function(data) {
		enableUploadOnlyToday()
	})
	socket.on('disable_upload_only_today', function(data) {
		disableUploadOnlyToday()
	})
	socket.on('check_upload_only_today', function(data) {
		uploadOnlyTodayEnabled(function(response){
			data(response);
			
		});
	});

});

http.listen(80, function() {
  	console.log('listening on *:80');
});

var main_upload_status;

function checkups() {
	main_upload.main_upload_status( function(status) {
		if(main_upload_status != status) {
			main_upload_status = status;
			console.log(main_upload_status);
		}
	});

}
var upload_status = "none"
var check_usb_process = setInterval(function(){
	autoUploadEnabled(function(is_enabled){
		if(is_enabled && upload_status === "none"){
			secret = get_secret()
			var dbx = new Dropbox({ accessToken: secret.token });
			dbx.usersGetCurrentAccount().
			then(function(response){
				console.log("got dropbox account");
				dir.get_usb_dev_path(function(usb_dev_path){
					dir.setup_usb(function(status) {
						dir_driver.get_usb(function(response){
							console.log(response)
							if(response === "0"){
								console.log("none found")
							}else{
								console.log("clearing_usb_checking_process"+response)
								uploadAllFiles();
								upload_status = "done";
								
							}

						})
					})
				})
			}).
			catch(function(error){ 
				console.error("No account setup");
				return;
				
			}); 
			
		}else{
			console.log("upload donezo");
			dir_driver.get_usb(function(response){
				console.log(response);
				if(response === "0"){
					console.log("usb unpluggeD")
					upload_status = "none";
				}
			});
		}
	})
}, 20000);
var checkups_process = setInterval(checkups, 100);

